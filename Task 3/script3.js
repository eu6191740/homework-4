let deposit = document.querySelector(".input-deposit");
let percent = document.querySelector(".input-percent");
let year = document.querySelector(".input-year");
let calculate = document.querySelector(".calc");
let amount = document.querySelector(".amount");

calculate.addEventListener("click", () => {
    amount.innerHTML = deposit.value * (1 + ((percent.value * year.value) / 100))
})